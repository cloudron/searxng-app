# Searx for Cloudron
A privacy-respecting, hackable metasearch engine

- developped by : [Adam Tauber](https://github.com/asciimoo/searx/)
- forked from : [Joey / Searx-app](https://git.cloudron.io/joey/searx-app/)

## History
The success of this app belong to [Joey](https://git.cloudron.io/joey/searx-app/) who, pretty much, build it.  I added few pieces on the puzzle and maintain it.  

### Goal
Expand the List of running instances : https://github.com/asciimoo/searx/wiki/Searx-instances  