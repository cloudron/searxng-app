FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# https://github.com/searxng/searxng/issues/915 (rolling release)
# renovate: datasource=git-refs packageName=https://github.com/searxng/searxng branch=master
ARG SEARX_COMMIT=8b1d73c5b96e9059c397b62f084aa8cee378218b
                  
RUN curl -L https://github.com/searxng/searxng/archive/${SEARX_COMMIT}.tar.gz | tar -xz --strip-components 1 -f -
RUN python3 -m venv /app/code/venv && \
    . /app/code/venv/bin/activate && \
    pip3 install --no-cache -r /app/code/requirements.txt

RUN mv /app/code/searx/settings.yml /app/code/searx/settings.yml.orig && \
    ln -sf /app/data/settings.yml /app/code/searx/settings.yml

# create version_frozen.py . this is just for display purposes in https://github.com/searxng/searxng/blob/bf75a8c2a015/searx/version.py#L111
RUN date --iso-8601 > /app/pkg/build-date.txt
RUN echo -e "VERSION_STRING = '$(cat /app/pkg/build-date.txt)-${SEARX_COMMIT:0:8}'\nVERSION_TAG = '${SEARX_COMMIT:0:8}'\nDOCKER_TAG = '${VERSION:0:8}'\nGIT_URL = 'https://github.com/searxng/searxng/'\nGIT_BRANCH = 'master'" >> /app/code/searx/version_frozen.py

RUN chown -R cloudron:cloudron /app/code

# Fix python UnicodeDecodeError
ENV LANG C.UTF-8

COPY start.sh /app/pkg/start.sh

CMD [ "/app/pkg/start.sh" ]
