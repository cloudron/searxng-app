#!/bin/bash

set -eu

if [[ ! -f "/app/data/settings.yml" ]]; then
    echo "=> Detected first run"
    cp /app/code/searx/settings.yml.orig /app/data/settings.yml
    yq eval -i ".server.secret_key=\"$(openssl rand -hex 16)\"" /app/data/settings.yml
fi

yq eval -i ".server.port=8888" /app/data/settings.yml
yq eval -i ".server.bind_address=\"0.0.0.0\"" /app/data/settings.yml

chown -R cloudron:cloudron /app/data

echo "==> Starting SearxNG"
source /app/code/venv/bin/activate
exec gosu cloudron python3 -m searx.webapp
