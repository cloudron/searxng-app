#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    let athenticated = false;

    let browser, app;
    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}/login`);

        await browser.wait(until.elementLocated(By.id('loginProceedButton')));
        await browser.findElement(By.id('loginProceedButton')).click();
        if (!athenticated) {
            await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), 5000);
            await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
            await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.id('loginSubmitButton')).click();
            athenticated = true;
        }
        await browser.wait(until.elementLocated(By.id('q')), 5000);
        await browser.sleep(2000);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);
        await browser.sleep(2000); // wait for "connection lost" error
        await browser.get(`https://${app.fqdn}/login`);
        await browser.wait(until.elementLocated(By.id('loginProceedButton')), TIMEOUT);
    }

    async function search() {
        await browser.get(`https://${app.fqdn}`);
        await browser.wait(until.elementLocated(By.id('q')), 5000);
        await browser.findElement(By.id('q')).sendKeys('cloudron');
        await browser.findElement(By.id('q')).sendKeys(Key.RETURN);
        await browser.wait(until.elementLocated(By.xpath('//span[text()="Cloudron"]')), 5000);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app (no sso)', function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can search', search);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    it('install app (sso)', function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, username, password));
    it('can search', search);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login.bind(null, username, password));
    it('can search', search);
    it('can logout', logout);

    it('can restart app', function () {
        execSync('cloudron restart --app ' + app.id);
    });

    it('can login', login.bind(null, username, password));
    it('can search', search);
    it('can logout', logout);

    it('move to different location', function () {
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can login', login.bind(null, username, password));
    it('can search', search);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install from appstore', function () {
        execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
    });

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });

    it('can login', login.bind(null, username, password));
    it('can search', search);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
