### About

SearXNG is a free internet metasearch engine which aggregates results from various search services and databases. Users are neither tracked nor profiled. 

SearXNG is a fork of searx.

### How does SearXNG protect privacy?

SearXNG protects the privacy of its users in multiple ways regardless of the type of the instance (private, public). Removal of private data from search requests comes in three forms:

* removal of private data from requests going to search services
* not forwarding anything from a third party services through search services (e.g. advertisement)
* removal of private data from requests going to the result pages

### Donate

If you want to support the SearXNG team you can make a [donation](https://docs.searxng.org/donate.html).

